﻿using RobinBird.FirebaseTools.Storage.Addressables;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

public class SceneLoadTester : MonoBehaviour
{
    [SerializeField]
    private AssetReference sceneReference;
    
    void Awake()
    {
        FirebaseAddressablesManager.FirebaseSetupFinished += () =>
        {
            Addressables.LoadSceneAsync(sceneReference, LoadSceneMode.Additive).Completed += handle =>
            {
                Debug.Log("Scene loaded successfully");
            };
        };
    }
}
